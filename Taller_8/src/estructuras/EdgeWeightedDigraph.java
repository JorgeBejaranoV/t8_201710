package estructuras;


import java.util.Stack;



public class EdgeWeightedDigraph {


	private final int V;                
	private int E;                      
	private Bag<DirectedEdge>[] adj;    
	private int[] numeroAdyacentes;             


	public EdgeWeightedDigraph(int V) {
		
		this.V = V;
		this.E = 0;
		this.numeroAdyacentes = new int[V];
		for (int v = 0; v < V; v++)
			adj[v] = new Bag<DirectedEdge>();
	}

	public EdgeWeightedDigraph(int V, int E) {
		this(V);
		
		for (int i = 0; i < E; i++) {
			int v = StdRandom.uniform(V);
			int w = StdRandom.uniform(V);
			double peso = 0.01 * StdRandom.uniform(100);
			DirectedEdge e = new DirectedEdge(v, w, peso);
			addEdge(e);
		}
	}


	public EdgeWeightedDigraph(EdgeWeightedDigraph G) {
		this(G.V());
		this.E = G.E();
		for (int v = 0; v < G.V(); v++)
			this.numeroAdyacentes[v] = G.numeroAdyacentes(v);
		for (int v = 0; v < G.V(); v++) {
			Stack<DirectedEdge> inverso = new Stack<DirectedEdge>();
			for (DirectedEdge e : G.adj[v]) {
				inverso.push(e);
			}
			for (DirectedEdge e : inverso) {
				adj[v].add(e);
			}
		}
	}


	public int V() {
		return V;
	}


	public int E() {
		return E;
	}


	public void addEdge(DirectedEdge e) {
		int v = e.from();
		int w = e.to();
		adj[v].add(e);
		numeroAdyacentes[w]++;
		E++;
	}



	public Iterable<DirectedEdge> adj(int v) {

		return adj[v];
	}


	public int outdegree(int v) {

		return adj[v].size();
	}


	public int numeroAdyacentes(int v) {

		return numeroAdyacentes[v];
	}

	
	public Iterable<DirectedEdge> edges() {
		Bag<DirectedEdge> list = new Bag<DirectedEdge>();
		for (int v = 0; v < V; v++) {
			for (DirectedEdge e : adj(v)) {
				list.add(e);
			}
		}
		return list;
	} 

	protected class BreadthFirstDirectedPaths {
		private static final int INFINITY = Integer.MAX_VALUE;
		private boolean[] marked;  
		private int[] enlazado;     
		private int[] distancia;      

		public BreadthFirstDirectedPaths( EdgeWeightedDigraph G, int s) {
			marked = new boolean[G.V()];
			distancia = new int[G.V()];
			enlazado = new int[G.V()];
			for (int v = 0; v < G.V(); v++)
				distancia[v] = INFINITY;
		
			bfs(G, s);
		}


		public BreadthFirstDirectedPaths( EdgeWeightedDigraph G, Iterable<Integer> sources) {
			marked = new boolean[G.V()];
			distancia = new int[G.V()];
			enlazado = new int[G.V()];
			for (int v = 0; v < G.V(); v++)
				distancia[v] = INFINITY;
			validateVertices(sources);
			bfs(G, sources);
		}

		// BFS from single source
		private void bfs( EdgeWeightedDigraph G, int s) {
			Queue<Integer> q = new Queue<Integer>();
			marked[s] = true;
			distancia[s] = 0;
			q.enqueue(s);
			while (!q.isEmpty()) {
				int v = q.dequeue();
				for (int w : G.adj(v)) {
					if (!marked[w]) {
						enlazado[w] = v;
						distancia[w] = distancia[v] + 1;
						marked[w] = true;
						q.enqueue(w);
					}
				}
			}
		}


		private void bfs( EdgeWeightedDigraph G, Iterable<Integer> sources) {
			Queue<Integer> q = new Queue<Integer>();
			for (int s : sources) {
				marked[s] = true;
				distancia[s] = 0;
				q.enqueue(s);
			}
			while (!q.isEmpty()) {
				int v = q.dequeue();
				for (int w : G.adj(v)) {
					if (!marked[w]) {
						enlazado[w] = v;
						distancia[w] = distancia[v] + 1;
						marked[w] = true;
						q.enqueue(w);
					}
				}
			}
		}


		public boolean estaConectado(int v) {
		
			return marked[v];
		}

		public int distancia(int v) {
		
			return distancia[v];
		}
		public Iterable<Integer> pathTo(int v) {

			if (!estaConectado(v)) return null;
			Stack<Integer> path = new Stack<Integer>();
			int x;
			for (x = v; distancia[x] != 0; x = enlazado[x])
				path.push(x);
			path.push(x);
			return path;
		}

		
		

	
	}

	
}
//Basado en los algoritmos del libro Algorithms 4th edition - Sedgewick
