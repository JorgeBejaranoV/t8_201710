package estructuras;

public class DFS {
	protected class DirectedDFS{
		private boolean[] marked;  
		private int count;        


		public DirectedDFS(EdgeWeightedDigraph G, int s) {
			marked = new boolean[G.V()];

			dfs(G, s);
		}

		public DirectedDFS(EdgeWeightedDigraph G, Iterable<Integer> sources) {
			marked = new boolean[G.V()];
			for (int v : sources) {
				if (!marked[v]) dfs(G, v);
			}
		}

		private void dfs(EdgeWeightedDigraph G, int v) { 
			count++;
			marked[v] = true;
			for (int w : G.adj(v)) {
				if (!marked[w]) dfs(G, w);
			}
		}

	
	
		public int count() {
			return count;
		}

		public boolean marked(int v) {
			return marked[v];
		}

	}
}
// Basado en los algoritmos del libro Algorithms 4th edition - Sedgewick
